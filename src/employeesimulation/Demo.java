/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose IncomeTaxCalculator | Templates
 * and open the template in the editor.
 */
package employeesimulation;

import java.util.Date;

public class Demo {
    
    public static void main(String[] args){
    
    Employee employee = new Employee("12345", "Jack Johnson", new Date(1995, 07, 19, 8, 30, 0));
    
    boolean thisYearPromotionValid = employee.isPromotionDueThisYear();
    
    IncomeTaxCalculator incomeTaxCalculator = new IncomeTaxCalculator();
    
    double currentYearIncomeTax = incomeTaxCalculator.calcIncomeTaxForCurrentYear(employee);
  
        System.out.println( employee + " This Year Promotion Valid:  " + thisYearPromotionValid + 
                " Income Tax: $" + currentYearIncomeTax);
  }
}
