/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeesimulation;

import java.util.Date;

public class Employee {
    
    private String employeeId;
    private String name;
    private Date dateOfJoining;
    
    public Employee(String employeeId, String name, Date dateOfJoining){
        this.employeeId = employeeId;
        this.name = name;
        this.dateOfJoining = dateOfJoining;
    }
    
    public String getEmployeeId(){
        return employeeId;
    }
    
    public String getName(){
        return name;
    }
      
    public Date getDateOfJoining(){
        return dateOfJoining;
    } 
    
    public boolean isPromotionDueThisYear(){
        return true;
    }
    
    public String toString(){
        return "Employee Name: " + getName() + " Employee ID: " + getEmployeeId();
    }
}
